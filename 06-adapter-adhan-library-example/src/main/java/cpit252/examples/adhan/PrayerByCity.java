package cpit252.examples.adhan;

public interface PrayerByCity {
    String getPrayerTimes(String city);
}
