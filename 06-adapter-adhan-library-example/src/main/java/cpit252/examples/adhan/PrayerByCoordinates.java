package cpit252.examples.adhan;

public interface PrayerByCoordinates {

    String getPrayerTimes(double lat, double lon);
}
