package transmissions;

public interface Transmission {
    public String getTransmissionType();

    public int getNumberOfSpeeds();
}
