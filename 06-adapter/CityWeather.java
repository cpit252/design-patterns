package weather;

public interface CityWeather {
  public double getTemp(String city);
}
