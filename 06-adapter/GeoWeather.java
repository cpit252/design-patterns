package weather;

public interface GeoWeather {
  public double getTemp(double latitude, double longitude);
}
