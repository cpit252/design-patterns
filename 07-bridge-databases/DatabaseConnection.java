public interface DatabaseConnection {
    DatabaseConnection connect();
}