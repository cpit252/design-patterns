# Proxy Design Pattern Example
This is an example of the Proxy design pattern where we protect a weather service behind a proxy that checks for API keys.

![](./proxy-weather-service-example.png)

