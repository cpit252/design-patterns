public interface WeatherService {
    String getWeather(String city);
}