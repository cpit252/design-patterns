public class Veggie extends Sandwich {

    public Veggie() {
        super.description = "Veggie Sandwich";
    }

    public double cost() {
        return 2.99;
    }
}