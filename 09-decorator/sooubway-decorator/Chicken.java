public class Chicken extends Sandwich {

    public Chicken() {
        super.description = "Chicken Sandwich";
    }

    public double cost() {
        return 3.99;
    }
}