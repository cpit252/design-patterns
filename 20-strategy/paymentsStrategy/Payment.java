package paymentsStrategy;

public interface Payment {
    void pay(double amount);
}
